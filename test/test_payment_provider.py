import unittest
from src.payment_provider import PaymentProvider


class PaymentProviderTest(unittest.TestCase):
    
    def test_stolen_card(self):
        payment = PaymentProvider('4000000000009979', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'failed')
        self.assertEqual(payment.get_outcome()['network_status'], 'declined_by_network')
        self.assertEqual(payment.get_outcome()['reason'], 'stolen_card')
        self.assertNotEqual(payment.get_error(), {})

    def test_insufficient_funds(self):
        payment = PaymentProvider('4000000000009995', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'failed')
        self.assertEqual(payment.get_outcome()['network_status'], 'declined_by_network')
        self.assertEqual(payment.get_outcome()['reason'], 'insufficient_funds')
        self.assertNotEqual(payment.get_error(), {})

    def test_lost_card(self):
        payment = PaymentProvider('4000000000009987', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'failed')
        self.assertEqual(payment.get_outcome()['network_status'], 'declined_by_network')
        self.assertEqual(payment.get_outcome()['reason'], 'lost_card')
        self.assertNotEqual(payment.get_error(), {})

    def test_invalid_credit_card_number(self):
        payment = PaymentProvider('40000007600000', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'failed')
        self.assertEqual(payment.get_outcome()['network_status'], 'declined_by_network')
        self.assertEqual(payment.get_outcome()['reason'], 'Invalid Primary Account Number provided')
        self.assertNotEqual(payment.get_error(), {})

    def test_fradulent(self):
        payment = PaymentProvider('4100000000000019', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'blocked')
        self.assertEqual(payment.get_outcome()['network_status'], 'not_sent_to_network')
        self.assertEqual(payment.get_outcome()['reason'], 'highest_risk_level')
        self.assertNotEqual(payment.get_error(), {})

    def test_successful(self):
        payment = PaymentProvider('4242424242424242', '999', 'usd')
        payment.charge()
        self.assertEqual(payment.get_status(), 'succeeded')
        self.assertEqual(payment.get_outcome()['network_status'], 'approved_by_network')
        self.assertEqual(payment.get_error(), {})

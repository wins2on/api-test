import requests


class PaymentProvider:

    def __init__(self, credit_card_id, amount, currency):
        self.credit_card_id = credit_card_id
        self.amount = amount
        self.currency = currency
        self.request = {}

    def populate_status(self):
        if 'status' not in self.request:
            self.request['status'] = 'failed'

    def populate_outcome(self):
        if 'outcome' not in self.request:
            self.request['outcome'] = {}
            self.request['outcome']['network_status'] = 'declined_by_network'
            self.request['outcome']['reason'] = self.request['error']['message']
            self.request['outcome']['type'] = self.request['error']['type']

    def populate_error(self):
        if 'error' not in self.request:
            self.request['error'] = {}
        if self.request['status'] == 'failed' or self.request['status'] == 'blocked':
            self.request['error'] = self.request['outcome']['reason']
            self.request['error'] = self.request['outcome']['type']

    def get_status(self):
        return self.request['status']

    def get_outcome(self):
        return self.request['outcome']

    def get_error(self):
        return self.request['error']

    def charge(self):
        API_ENDPOINT = "http://5df9c4eee9f79e0014b6b2eb.mockapi.io/charge/" + self.credit_card_id
        data = {}
        data['amount'] = self.amount
        data['currency'] = self.currency
        self.request = requests.post(url = API_ENDPOINT, data = data).json()
        self.populate_status()
        self.populate_outcome()
        self.populate_error()
        return self.request
